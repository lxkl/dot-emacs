;;;
;;; Basic Configuration
;;;

;; default fonts

(set-face-attribute 'default nil :font "JetBrains Mono 9" :weight 'normal)
(set-face-attribute 'fixed-pitch nil :font "JetBrains Mono 9" :weight 'normal)
(set-face-attribute 'variable-pitch nil :font "DejaVu Serif 11")
(advice-add 'load-theme :after #'(lambda (&rest args) (set-face-attribute 'bold nil :weight 'extra-bold)))

;; global environment

(setenv "EDITOR" "emacsclient")

;; packages

(require 'package)

;; remove shortcuts

(global-unset-key (kbd "C-x C-c"))
(global-unset-key (kbd "C-x C-z"))
(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "<insert>"))

(dolist (hook/map '((magit-mode-hook . magit-mode-map)
                    (magit-mode-hook . magit-hunk-section-map)
                    (magit-mode-hook . magit-file-section-map)
                    (org-mode-hook . org-mode-map)))
  (add-hook (car hook/map) `(lambda () (define-key ,(cdr hook/map) (kbd "C-<return>") nil))))

(add-hook 'diff-mode-hook (lambda () (define-key diff-mode-map (kbd "M-o") nil)))

;; clean up display

(when (display-graphic-p) (tool-bar-mode -1) (scroll-bar-mode -1) (tooltip-mode -1))
(menu-bar-mode -1)
(blink-cursor-mode 0)
(setq-default cursor-type 'box)
(setq warning-suppress-types '((comp)))

;; disable confirmations

(fset 'yes-or-no-p 'y-or-n-p)
(setq kill-buffer-query-functions (delq 'process-kill-buffer-query-function kill-buffer-query-functions))
(setq dired-deletion-confirmer '(lambda (x) t))
(setq confirm-kill-processes nil)

;; files

(setq require-final-newline t)
(setq make-backup-files nil)
(setq vc-follow-symlinks t)
(global-auto-revert-mode 1)
(setq auto-revert-verbose nil)
(add-hook 'dired-mode-hook 'auto-revert-mode)
(setq dired-dwim-target t)
(add-hook 'before-save-hook (lambda () (whitespace-cleanup)))

;; display functions

(setq line-number-mode t)
(setq column-number-mode t)
(global-hl-line-mode 1)
(show-paren-mode 1)
(setq show-paren-style 'mixed)
(setq auto-window-vscroll nil)
(setq bidi-inhibit-bpa t)
(setq-default bidi-paragraph-direction 'left-to-right)

;; edit functions

(setq-default indent-tabs-mode nil)
(c-set-offset 'access-label 0)
(c-set-offset 'innamespace 0)
(defun lxkl/indent-buffer ()
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max) nil)))
(global-set-key (kbd "C-z <tab>") 'lxkl/indent-buffer)
(global-set-key (kbd "C-z j") 'join-line)

;; eshell

(global-set-key (kbd "C-z s") (lambda () (interactive)
                                (let ((current-prefix-arg '(4)))
                                  (call-interactively 'eshell))))
(add-hook 'eshell-mode-hook
          (lambda ()
            (setenv "PAGER" "cat")
            (eshell/alias "ll" "ls -h -l -A -F $*")))
(setq eshell-kill-processes-on-exit t)

;; ispell

(global-set-key (kbd "C-z C-d") 'lxkl/cycle-dictionary)
(global-set-key (kbd "C-z d") 'lxkl/ispell-save-word)

(defvar lxkl/dictionaries-ring nil "Dictionaries ring for Ispell")
(let ((dictionaries '("german" "english")))
  (setq lxkl/dictionaries-ring (make-ring (length dictionaries)))
  (dolist (x dictionaries) (ring-insert lxkl/dictionaries-ring x)))
(defun lxkl/cycle-dictionary ()
  (interactive)
  (let ((x (ring-ref lxkl/dictionaries-ring -1)))
    (ring-insert lxkl/dictionaries-ring x)
    (ispell-change-dictionary x)
    (message "Changed dictionary to %s." x)))
(ispell-change-dictionary (ring-ref lxkl/dictionaries-ring -2))
(setq ispell-silently-savep t)
(defun lxkl/ispell-save-word ()
  (interactive)
  (let ((current-location (point))
        (word (flyspell-get-word)))
    (when (consp word)
      (flyspell-do-correct 'save nil
                           (car word)
                           current-location
                           (cadr word)
                           (caddr word)
                           current-location))))

;; mailcap

(setq
 mailcap-mime-data
 '(("application"
    ("pdf" (viewer . "evince %s") (type . "application/pdf"))
    ("vnd\.openxmlformats-officedocument\..*" (viewer . "soffice %s") (type . "application/vnd.openxmlformats-officedocument.*"))
    ("msword" (viewer . "soffice %s") (type . "application/msword")))
   ("image" (".*" (viewer . "eog %s") (type . "image/*")))))

;; restart or exit emacs

(global-set-key (kbd "C-z C-c C-c") 'restart-emacs)
(global-set-key (kbd "C-x C-c C-c") 'save-buffers-kill-terminal)

;; server

(load "server")
(unless (server-running-p) (server-start))

;;;
;;; Packages
;;;

(eval-when-compile
  (require 'use-package))

(use-package ace-window
  :bind
  (("M-o" . ace-window)))

(use-package avy
  :bind
  ("C-z z" . avy-goto-char-2))

(use-package beacon
  :demand
  :config
  (beacon-mode 1))

(use-package company
  :config
  (setq company-global-modes '(not eshell-mode inferior-ess-mode))
  (global-company-mode t))

(use-package counsel
  :bind
  (("C-x C-f" . counsel-find-file)
   ("M-x" . counsel-M-x)
   ("M-y" . counsel-yank-pop)
   ("C-z g g" . counsel-git-grep)
   ("C-z g G" . lxkl/git-grep-here))
  :config
  (defun lxkl/git-grep-here ()
    (interactive)
    (counsel-git-grep nil (file-name-directory (buffer-file-name)))))

(use-package dired-du
  :config
  (setq dired-du-size-format 'comma))

(use-package doom-themes
  :config
  (setq doom-one-brighter-comments t)
  (load-theme 'doom-one-light t))

(use-package hydra
  :config
  (defhydra lxkl/hydra-kill-buffers ()
    "kill buffers"
    ("k" (kill-buffer (current-buffer)) "kill")
    ("n" next-buffer "next")
    ("q" nil "quit"))
  (global-set-key (kbd "C-z k") 'lxkl/hydra-kill-buffers/body))

(use-package ivy
  :bind
  (("C-<return>" . ivy-switch-buffer))
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t))

(unless (and (require 'latex nil t) (require 'reftex nil t))
  (if (fboundp 'straight-use-package)
      (straight-use-package 'auctex)
    (use-package auctex)))
(require 'latex)
(defun lxkl/latex-beamer-template-frame ()
  "Create a simple template and move point to after \\frametitle."
  (interactive)
  (LaTeX-environment-menu "frame")
  (insert "\\frametitle{}")
  (backward-char 1))
(defun lxkl/latex-insert-environment (name &optional extra)
  (insert "\\begin{" name "}" extra "\n	\n\\end{" name "}")
  (previous-line))
(defun lxkl/latex-insert-environment-IEEEeqnarray ()
  (interactive) (lxkl/latex-insert-environment "IEEEeqnarray*" "{c}"))
(define-key LaTeX-mode-map (kbd "C-z f") 'lxkl/latex-beamer-template-frame)
(define-key LaTeX-mode-map (kbd "C-z e") 'lxkl/latex-insert-environment-IEEEeqnarray)
(define-key LaTeX-mode-map (kbd "C-z C-e") 'lxkl/latex-insert-environment-IEEEeqnarray)
(setq font-latex-fontify-script nil)
(setq font-latex-fontify-sectioning 'color)
(setq font-latex-fontify-sectioning 1.2)
(setq TeX-open-quote "\\enquote{")
(setq TeX-close-quote "}")
(setf (alist-get 5 LaTeX-font-list) '("\\emphasis{" "}"))
(setq LaTeX-verbatim-environments-local '("Verbatim" "lstlisting"))
(setq TeX-electric-sub-and-superscript t)
(LaTeX-add-environments '("IEEEeqnarray" LaTeX-env-label) '("IEEEeqnarray*" LaTeX-env-label))
(setq font-latex-math-environments '("IEEEeqnarray" "IEEEeqnarray*"))
(setq texmathp-tex-commands '(("IEEEeqnarray" env-on) ("IEEEeqnarray*" env-on)))
(require 'reftex)
(add-to-list 'reftex-ref-style-default-list "Hyperref")
(setq reftex-label-alist '(AMSTeX ("IEEEeqnarray" ?e "eq:" "~\\eqref{%s}" t nil)))
(add-hook 'LaTeX-mode-hook (lambda () (turn-on-reftex)))

(use-package magit
  :bind
  (("C-z g s" . magit-status)
   ("C-z g l" . magit-list-repositories))
  :config
  (setq magit-diff-refine-hunk 'all)
  (setq magit-repository-directories '(("~/repos/" . 1)))
  (add-to-list 'magit-repolist-columns '("Dirty" 5 magit-repolist-column-dirty ((:right-align t)))))

(use-package markup-faces
  :load-path "markup-faces"
  :defer t)

(use-package neotree
  :bind
  (("C-z n t" . neotree-toggle)
   ("C-z n n" . neotree-refresh))
  :config
  (setq neo-theme 'nerd)
  (setq neo-autorefresh nil))

(use-package olivetti
  :demand
  :diminish
  :config
  (setq olivetti-minimum-body-width 100)
  :bind ("C-z O" . olivetti-mode))

(use-package projectile
  :bind-keymap
  (("C-z p" . projectile-command-map))
  :bind
  (("C-S-<return>" . projectile-switch-to-buffer))
  (:map projectile-command-map
        ("P" . lxkl/projectile-switch-project-find-file))
  :config
  (projectile-mode 1)
  (defun lxkl/projectile-switch-project-find-file ()
    (interactive)
    (let ((projectile-switch-project-action 'projectile-find-file))
      (projectile-switch-project)))
  (setq projectile-switch-project-action 'projectile-switch-to-buffer)
  (setq projectile-completion-system 'ivy))

(use-package rainbow-delimiters
  :config
  (add-hook 'LaTeX-mode-hook (lambda () (rainbow-delimiters-mode 1))))

(use-package smart-mode-line
  :config
  (setq sml/no-confirm-load-theme t)
  (setq sml/theme 'respectful)
  (sml/setup))

(use-package so-long
  :config
  (global-so-long-mode 1))

(use-package swiper
  :bind
  (("C-s" . swiper)))
