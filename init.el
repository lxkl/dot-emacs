;;;
;;; Basic Configuration
;;;

(setq gc-cons-threshold (* 100 1000 1000))

;; default fonts

(when (file-exists-p "/etc/opt/95-snyder-foreign-fonts.conf")
  (set-face-attribute 'default nil :font "Iosevka Term SS15 11" :weight 'normal)
  (set-face-attribute 'fixed-pitch nil :font "Iosevka Term SS15 11" :weight 'normal)
  (set-face-attribute 'variable-pitch nil :font "DejaVu Serif 11" :weight 'normal))
(set-face-attribute 'mode-line nil :height 0.9)
(set-face-attribute 'mode-line-inactive nil :height 0.9)

;; exec-path helpers
;;
;; Adding the redundant /bin at the end is a workaround; it seems that
;; otherwise the last item in the path is not used.

(setq lxkl/exec-path '())
(defun lxkl/exec-path-append (&rest args)
  (setq lxkl/exec-path (append lxkl/exec-path args)))
(defun lxkl/exec-path-prepend (&rest args)
  (setq lxkl/exec-path (append args lxkl/exec-path)))
(defun lxkl/exec-path-energize ()
  (setq exec-path (append (mapcar 'expand-file-name lxkl/exec-path) '("/bin")))
  (setenv "PATH" (mapconcat 'identity exec-path ":")))

;; global environment
(lxkl/exec-path-append "/usr/local/bin" "/usr/bin" "/bin" "/opt/lxkl/svtools" "/opt/lxkl/minisite" "/opt/lxkl/sword-tools" "/opt/lxkl/pubtools")
(lxkl/exec-path-energize)
(setenv "EDITOR" "emacsclient")
(setenv "SVTOOLS_SCAN_DIR" "/home/user/s6/service")
(setenv "SVTOOLS_SERVICE_REPOS" "/home/user/s6/sv")

;; packages

(require 'package)

;; remove shortcuts

(global-unset-key (kbd "C-x C-c"))
(global-unset-key (kbd "C-x C-z"))
(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "<insert>"))
(global-unset-key (kbd "<delete>"))
(global-unset-key (kbd "<home>"))
(global-unset-key (kbd "<end>"))
(global-unset-key (kbd "<f13>"))
(global-unset-key (kbd "<f14>"))
(global-unset-key (kbd "<f15>"))
(global-unset-key (kbd "<f16>"))

(defun lxkl/cleanup-C-return (hook/map)
  (add-hook (car hook/map) `(lambda () (define-key ,(cdr hook/map) (kbd "C-<return>") nil))))
(mapcar #'lxkl/cleanup-C-return
        '((cider-mode-hook . cider-mode-map)
          (cider-repl-mode-hook . cider-repl-mode-map)
          (ess-mode-hook . ess-mode-map)
          (ess-help-mode-hook . ess-help-mode-map)
          (magit-mode-hook . magit-mode-map)
          (magit-mode-hook . magit-hunk-section-map)
          (magit-mode-hook . magit-file-section-map)
          (org-mode-hook . org-mode-map)
          (slime-repl-mode-hook . slime-repl-mode-map)))

(add-hook 'diff-mode-hook (lambda () (define-key diff-mode-map (kbd "M-o") nil)))

;; clean up display

(when (display-graphic-p) (tool-bar-mode -1) (scroll-bar-mode -1) (tooltip-mode -1))
(menu-bar-mode -1)
(blink-cursor-mode 0)
(setq-default cursor-type 'box)
(setq warning-suppress-types '((comp)))

(defun lxkl/frame-redraw ()
  (interactive)
  (make-frame)
  (delete-frame))
(global-set-key (kbd "<f13> r") 'lxkl/frame-redraw)

;; disable confirmations

(fset 'yes-or-no-p 'y-or-n-p)
(setq kill-buffer-query-functions (delq 'process-kill-buffer-query-function kill-buffer-query-functions))
(setq dired-deletion-confirmer '(lambda (x) t))
(setq confirm-kill-processes nil)

;; buffers

(global-set-key (kbd "S-<f9>") 'bury-buffer)
(global-set-key (kbd "<f9>") 'unbury-buffer)
(global-set-key (kbd "M-<f9>") (lambda () (interactive) (kill-buffer (current-buffer))))

;; files

(setq require-final-newline t)
(setq make-backup-files nil)
(setq vc-follow-symlinks t)
(global-auto-revert-mode 1)
(setq auto-revert-verbose nil)
(add-hook 'dired-mode-hook 'auto-revert-mode)
(setq dired-dwim-target t)
(add-hook 'before-save-hook (lambda () (whitespace-cleanup)))
(global-set-key (kbd "<f13> f") 'find-name-dired)
(setq find-name-arg "-iname")
(setq auto-revert-check-vc-info t)

;; display functions

(setq line-number-mode t)
(setq column-number-mode t)
(global-hl-line-mode 1)
(show-paren-mode 1)
(setq show-paren-style 'mixed)
(global-unset-key (kbd "C-l"))
(global-set-key (kbd "C-l C-l")
                (lambda () (interactive)
                  (recenter-top-bottom)
                  (font-lock-fontify-buffer)))
(setq auto-window-vscroll nil)
(setq bidi-inhibit-bpa t)
(setq-default bidi-paragraph-direction 'left-to-right)

;; https://stackoverflow.com/a/10541426/3531954
(defun lxkl/scroll-up-in-place (n)
  (interactive "p")
  (next-line n)
  (unless (eq (window-end) (point-max))
    (scroll-up n)))
(defun lxkl/scroll-down-in-place (n)
  (interactive "p")
  (previous-line n)
  (unless (eq (window-start) (point-min))
    (scroll-down n)))
(global-set-key (kbd "C-S-e") 'lxkl/scroll-up-in-place)
(global-set-key (kbd "C-S-y") 'lxkl/scroll-down-in-place)

;; edit functions

(setq-default indent-tabs-mode nil)
(c-set-offset 'access-label 0)
(c-set-offset 'innamespace 0)
(defun lxkl/indent-buffer ()
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max) nil)))
(global-set-key (kbd "<f13> <tab>") 'lxkl/indent-buffer)
(global-set-key (kbd "<f13> j") 'join-line)
(defun lxkl/unfill-paragraph (&optional region)
  (interactive)
  (let ((fill-column (point-max)))
    (fill-paragraph nil region)))
(global-set-key [(meta shift q)] 'lxkl/unfill-paragraph)
(global-set-key (kbd "<f13> w w") 'whitespace-cleanup)

(add-hook 'python-mode-hook
          (lambda ()
            (setq indent-tabs-mode t)
            (setq python-indent 4)
            (setq tab-width 4)))

(defun lxkl/move-line-up ()
  (interactive)
  (transpose-lines 1)
  (previous-line 2))
(defun lxkl/move-line-down ()
  (interactive)
  (next-line 1)
  (transpose-lines 1)
  (previous-line 1))
(global-set-key (kbd "C-S-p") 'lxkl/move-line-up)
(global-set-key (kbd "C-S-n") 'lxkl/move-line-down)

;; http://emacs.stackexchange.com/questions/1051/copy-region-from-emacs-without-newlines
(defun lxkl/copy-simple (&optional beg end)
  "Save the current region (or line) to the `kill-ring' after stripping extra whitespace and new lines."
  (interactive
   (if (region-active-p)
       (list (region-beginning) (region-end))
     (list (line-beginning-position) (line-end-position))))
  (let ((my-text (buffer-substring-no-properties beg end)))
    (with-temp-buffer
      (insert my-text)
      (goto-char 1)
      (while (looking-at "[ \t\n]")
        (delete-char 1))
      (let ((fill-column 9333999))
        (fill-region (point-min) (point-max)))
      (kill-ring-save (point-min) (point-max))
      (message "Copied %i chars." (buffer-size)))))
(global-set-key (kbd "<f13> c") 'lxkl/copy-simple)

(defun lxkl/frenchquote-region (begin end)
  (interactive "r")
  (save-excursion
    (goto-char end)
    (skip-chars-backward " \t\n\r")
    (insert "»")
    (setq new-end (point))
    (goto-char begin)
    (insert "«")
    (fill-region begin new-end)))
(global-set-key (kbd "<f13> q") 'lxkl/frenchquote-region)

(require 'pulse)
(setq lxkl/reading-mode-scroll-overlay nil)
(defun lxkl/reading-mode-delete-scroll-overlay ()
  (when lxkl/reading-mode-scroll-overlay (delete-overlay lxkl/reading-mode-scroll-overlay)))
(defun lxkl/reading-mode-update-scroll-overlay ()
  (lxkl/reading-mode-delete-scroll-overlay)
  (setq lxkl/reading-mode-scroll-overlay
        (save-excursion
          (move-to-window-line -1)
          (make-overlay (point-at-bol) (point-at-eol))))
  (overlay-put lxkl/reading-mode-scroll-overlay 'face 'shadow))
(defun lxkl/reading-mode-scroll (n fun)
  (interactive)
  (let ((pos (save-excursion
               (move-to-window-line n)
               (point-at-bol))))
    (funcall fun (/ (window-body-height) 3))
    (lxkl/reading-mode-update-scroll-overlay)
    (goto-char pos)
    (pulse-momentary-highlight-one-line (point))))
(defvar lxkl/reading-mode-map (make-sparse-keymap))
(define-key lxkl/reading-mode-map [next]
  (lambda () (interactive) (lxkl/reading-mode-scroll -1 #'scroll-up-command)))
(define-key lxkl/reading-mode-map [prior]
  (lambda () (interactive) (lxkl/reading-mode-scroll 1 #'scroll-down-command)))
(define-minor-mode lxkl/reading-mode
  ""
  :init-value nil
  :global nil
  :keymap lxkl/reading-mode-map
  (if lxkl/reading-mode
      (progn
        (setq-local lxkl/reading-mode-olivetti-minimum-body-width olivetti-minimum-body-width)
        (setq-local lxkl/reading-mode-line-spacing line-spacing)
        (setq-local olivetti-minimum-body-width 70)
        (olivetti-mode -1)
        (olivetti-mode 1)
        (variable-pitch-mode 1)
        (setq-local line-spacing 7)
        (lxkl/reading-mode-update-scroll-overlay))
    (setq-local olivetti-minimum-body-width lxkl/reading-mode-olivetti-minimum-body-width)
    (olivetti-mode -1)
    (olivetti-mode 1) ;; assuming lxkl/reading-mode will only be used on top of olivetti-mode
    (variable-pitch-mode -1) ;; no idea how to determine if this was on or off before; so assuming it was off
    (setq-local line-spacing lxkl/reading-mode-line-spacing)
    (lxkl/reading-mode-delete-scroll-overlay)))
(global-set-key (kbd "<f13> R") 'lxkl/reading-mode)

;; lisp development

(define-key emacs-lisp-mode-map (kbd "C-c C-k") 'eval-buffer) ;; like slime-compile-and-load-file
(require 'scheme)
(require 'cmuscheme)
(define-key scheme-mode-map (kbd "C-c C-k") (lambda () (interactive) (scheme-send-region (point-min) (point-max))))
(setq scheme-program-name "gosh -i")
(add-to-list 'Info-directory-list "/opt/lxkl/foreign/gauche/share/info")
(defun lxkl/disable-geiser-mode ()
  (add-hook 'after-change-major-mode-hook (lambda () (geiser-mode -1))))

;; ESS

(defun lxkl/load-local-ess ()
  (dolist (f features)
    (when (string-prefix-p "ess-" (symbol-name f))
      (unload-feature f t)))
  (add-to-list 'load-path "/opt/lxkl/foreign/ESS/lisp")
  (require 'ess-site)
  (require 'ess)
  (ess-disable-smart-S-assign)
  (setq ess-ask-for-ess-directory nil)
  (setq ess-startup-directory "~/")
  (mapcar #'lxkl/cleanup-C-return
          '((ess-mode-hook . ess-mode-map)
            (ess-help-mode-hook . ess-help-mode-map)))
  (setq tab-always-indent t))

;; themes

(cl-defun lxkl/load-theme-from-path (name &optional dir &key req (dirs '()))
  (dolist (d (if dir (cons dir dirs) dirs))
    (dolist (l '(load-path custom-theme-load-path))
      (unless (member d (symbol-value l)) (add-to-list l d))))
  (when req (unload-feature req t) (require req))
  (load-theme name t))

(defun lxkl/cycle-themes (prefix)
  (interactive)
  (let* ((themes (cl-remove-if-not (lambda (x) (string-prefix-p prefix (symbol-name x)))
                                   (custom-available-themes)))
         (current (car custom-enabled-themes))
         (current-index (or (and current (cl-position current themes)) 0))
         (next-theme (nth (mod (1+ current-index) (length themes)) themes)))
    (if themes
        (progn
          (mapc #'disable-theme custom-enabled-themes)
          (load-theme next-theme t)
          (message "Switched to theme: %s" next-theme))
      (message "No themes available."))))
(global-set-key (kbd "<f5>") (lambda () (interactive) (lxkl/cycle-themes "doom-")))
(global-set-key (kbd "S-<f5>") (lambda () (interactive) (lxkl/cycle-themes "")))

;; index file

(unless (file-exists-p "~/org") (make-directory "~/org"))
(defun lxkl/open-index () (interactive) (find-file "~/org/index.org"))
(global-set-key (kbd "C-<f13>") 'lxkl/open-index)
(when (file-exists-p "~/org/index.org")
  (setq initial-buffer-choice "~/org/index.org"))

;; mail

(setq mail-specify-envelope-from t)
(setq mail-envelope-from 'header)
(setq message-sendmail-envelope-from 'header)
(setq message-send-mail-function 'message-send-mail-with-sendmail)
(setq sendmail-program "/opt/lxkl/msmtpq/msmtpq-enqueue") ;; see https://gitlab.com/lxkl/msmtpq
(setq message-kill-buffer-on-exit t)
(defun lxkl/attach-marked-files (buffer)
  "Attach all marked files to BUFFER"
  (interactive "BAttach to buffer: ")
  (let ((files (dired-get-marked-files)))
    (with-current-buffer (get-buffer buffer)
      (dolist (file files)
        (if (file-regular-p file)
            (mml-attach-file file
                             (mm-default-file-encoding file)
                             nil "attachment")
          (message "skipping non-regular file %s" file)))))
  (switch-to-buffer buffer))

;; browse and lookup

(setq browse-url-browser-function 'browse-url-generic)
(setq browse-url-generic-program "chromium")

(global-set-key (kbd "<f13> l w") 'lxkl/lookup-word-wikipedia)
(global-set-key (kbd "<f13> l g") 'lxkl/lookup-word-google)
(global-set-key (kbd "<f13> l d") 'lxkl/lookup-word-dictcc)
(global-set-key (kbd "<f13> l m") 'lxkl/lookup-word-merriam-webster)
(global-set-key (kbd "<f13> l M") 'lxkl/lookup-word-merriam-webster-1828)
(global-set-key (kbd "<f13> l k") 'lxkl/lookup-word-kjv)
(defun lxkl/lookup-word-wikipedia () (interactive) (lxkl/lookup-word "https://en.wikipedia.org/wiki/"))
(defun lxkl/lookup-word-google () (interactive) (lxkl/lookup-word "https://www.google.com/search?q="))
(defun lxkl/lookup-word-dictcc () (interactive) (lxkl/lookup-word "https://www.dict.cc/?s="))
(defun lxkl/lookup-word-merriam-webster () (interactive) (lxkl/lookup-word "https://www.merriam-webster.com/dictionary/"))
(defun lxkl/lookup-word-merriam-webster-1828 () (interactive) (lxkl/lookup-word "https://1828.mshaffer.com/d/search/word,"))
(defun lxkl/lookup-word-kjv () (interactive) (lxkl/lookup-word "http://www.kingjamesbibledictionary.com/Dictionary/"))
(defun lxkl/lookup-word (service)
  "Look up the word under cursor using the specified service. If
there is a text selection (a phrase), use that. This command
switches to browser."
  (interactive)
  (let (word)
    (setq word (if (use-region-p)
                   (buffer-substring-no-properties (region-beginning) (region-end))
                 (current-word)))
    (setq word (replace-regexp-in-string " " "_" word))
    (browse-url (concat service word))))

;; eshell

(global-set-key (kbd "<f13> s") (lambda () (interactive)
                                  (let ((current-prefix-arg '(4)))
                                    (call-interactively 'eshell))))
(add-hook 'eshell-mode-hook
          (lambda ()
            (setenv "PAGER" "cat")
            (eshell/alias "cauvpn" "/opt/lxkl/setup-desktop/common/cauvpn")
            (eshell/alias "cauprint" "/opt/lxkl/setup-desktop/common/config-printer-simple sunms399")
            (eshell/alias "caupub" "/opt/lxkl/pubtools/pubwebdav.py -f /home/user/repos/pubwebdav/config.yaml $*")
            (eshell/alias "qgpg" "qubes-gpg-client $*")
            (eshell/alias "mr" "mr -d ~ $*")
            (eshell/alias "ll" "ls -h -l -A -F $*")))
(setq eshell-kill-processes-on-exit t)

;; ispell

(global-set-key (kbd "<f13> C-d") 'lxkl/cycle-dictionary)
(global-set-key (kbd "<f13> d") 'lxkl/ispell-save-word)

(defvar lxkl/dictionaries-ring nil "Dictionaries ring for Ispell")
(let ((dictionaries '("german" "english")))
  (setq lxkl/dictionaries-ring (make-ring (length dictionaries)))
  (dolist (x dictionaries) (ring-insert lxkl/dictionaries-ring x)))
(defun lxkl/cycle-dictionary ()
  (interactive)
  (let ((x (ring-ref lxkl/dictionaries-ring -1)))
    (ring-insert lxkl/dictionaries-ring x)
    (ispell-change-dictionary x)
    (message "Changed dictionary to %s." x)))
(ispell-change-dictionary (ring-ref lxkl/dictionaries-ring -2))
(setq ispell-silently-savep t)
(defun lxkl/ispell-save-word ()
  (interactive)
  (let ((current-location (point))
        (word (flyspell-get-word)))
    (when (consp word)
      (flyspell-do-correct 'save nil
                           (car word)
                           current-location
                           (cadr word)
                           (caddr word)
                           current-location))))

;; mailcap

(setq
 mailcap-mime-data
 '(("application"
    ("pdf" (viewer . "zathura %s") (type . "application/pdf"))
    ("vnd\.openxmlformats-officedocument\..*" (viewer . "soffice %s") (type . "application/vnd.openxmlformats-officedocument.*"))
    ("msword" (viewer . "soffice %s") (type . "application/msword")))
   ("image" (".*" (viewer . "eog %s") (type . "image/*")))))

;; simple memo printing

(setq ps-print-header nil
      ps-top-margin 50
      ps-right-margin 100
      ps-bottom-margin 80
      ps-left-margin 100)
(defun lxkl/print-org-section ()
  (interactive)
  (org-ascii-export-as-ascii nil t t t)
  (ps-print-buffer)
  (kill-buffer))

;; restart or exit emacs

(global-set-key (kbd "<f13> C-c C-c") 'restart-emacs)
(global-set-key (kbd "C-x C-c C-c")
                (lambda () (interactive)
                  (start-process "bye" nil "chpst" "-P" "gnome-terminal" "--" "sh" "-c" "sleep 2")
                  (sleep-for 1)
                  (save-buffers-kill-terminal)))

;; server

(load "server")
(unless (server-running-p) (server-start))

;; misc

(global-set-key (kbd "<print>") (lambda () (interactive) (shell-command "maim -u -s ~/tmp/_screenshot.png")))
(defun lxkl/pwgen ()
  (interactive)
  (let ((pw (string-trim-right (shell-command-to-string "pwgen -s 24 -B -N1") "\n")))
    (insert pw)
    (kill-new pw)))
(global-set-key (kbd "<f13> <f13> p") 'lxkl/pwgen)

;;;
;;; Packages
;;;

(eval-when-compile
  (require 'use-package))

(use-package ace-window
  :bind
  (("M-o" . ace-window)))

(use-package adoc-mode
  :load-path "adoc-mode"
  :mode ("\\.adoc\\'" "\\.asciidoc\\'"))

(use-package avy
  :bind
  ("<f13> z" . avy-goto-char-2))

(use-package beacon
  :demand
  :config
  (beacon-mode 1))

(use-package company
  :config
  (define-key company-active-map (kbd "C-n") 'company-select-next)
  (define-key company-active-map (kbd "C-p") 'company-select-previous)
  (setq company-global-modes '(not eshell-mode inferior-ess-mode))
  (global-company-mode t))

(use-package counsel
  :bind
  (("C-x C-f" . counsel-find-file)
   ("M-x" . counsel-M-x)
   ("M-y" . counsel-yank-pop)
   ("<f13> g g" . counsel-git-grep)
   ("<f13> g G" . lxkl/git-grep-here))
  :config
  (setq enable-recursive-minibuffers t)
  (defun lxkl/git-grep-here ()
    (interactive)
    (counsel-git-grep nil (file-name-directory (buffer-file-name)))))

(use-package dired-du
  :config
  (setq dired-du-size-format 'comma))

(use-package dtk
  :load-path "dtk"
  :bind (("<f13> b" . dtk-bible))
  :custom
  (dtk-default-module "KJV")
  (dtk-default-module-category "Biblical Texts")
  (dtk-word-wrap t))

(use-package ess
  :init (require 'ess-site)
  :commands R
  :config
  (ess-disable-smart-S-assign)
  (setq ess-ask-for-ess-directory nil)
  (setq ess-startup-directory "~/"))

(use-package geiser
  :load-path "/opt/lxkl/foreign/geiser/elisp"
  :config (setq geiser-default-implementation 'gauche))
(use-package geiser-gauche
  :load-path "/opt/lxkl/foreign/geiser-gauche")

(use-package hydra
  :config
  (defhydra lxkl/hydra-kill-buffers ()
    "kill buffers"
    ("k" (kill-buffer (current-buffer)) "kill")
    ("n" next-buffer "next")
    ("q" nil "quit"))
  (global-set-key (kbd "<f13> k") 'lxkl/hydra-kill-buffers/body))

(use-package ivy
  :bind
  (("C-<return>" . ivy-switch-buffer))
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t))

(unless (and (require 'latex nil t) (require 'reftex nil t))
  (if (fboundp 'straight-use-package)
      (straight-use-package 'auctex)
    (use-package auctex)))
(require 'latex)
(defun lxkl/latex-beamer-template-frame ()
  "Create a simple template and move point to after \\frametitle."
  (interactive)
  (LaTeX-environment-menu "frame")
  (insert "\\frametitle{}")
  (backward-char 1))
(defun lxkl/latex-insert-environment (name &optional extra)
  (insert "\\begin{" name "}" (or extra "") "\n	\n\\end{" name "}")
  (latex-indent)
  (previous-line))
(defun lxkl/latex-insert-environment-IEEEeqnarray ()
  (interactive) (lxkl/latex-insert-environment "IEEEeqnarray*" "{0l}"))
(defun lxkl/latex-insert-environment-line (name &optional extra)
  (insert "\\begin{" name "}" (or extra ""))
  (save-excursion (insert "\\end{" name "}")))
(defun lxkl/latex-insert-environment-itemeqn ()
  (interactive)
  (lxkl/latex-insert-environment-line "itemeqn"))
(define-key LaTeX-mode-map (kbd "<f13> f") 'lxkl/latex-beamer-template-frame)
(define-key LaTeX-mode-map (kbd "<f13> e") 'lxkl/latex-insert-environment-IEEEeqnarray)
(define-key LaTeX-mode-map (kbd "<f13> C-e") 'lxkl/latex-insert-environment-IEEEeqnarray)
(define-key LaTeX-mode-map (kbd "<f13> C-i") 'lxkl/latex-insert-environment-itemeqn)
(setq font-latex-fontify-script nil)
(setq font-latex-fontify-sectioning 'color)
(setq font-latex-fontify-sectioning 1.2)
(setq TeX-open-quote "\\enquote{")
(setq TeX-close-quote "}")
(setf (alist-get 5 LaTeX-font-list) '("\\emphasis{" "}"))
(setq LaTeX-verbatim-environments-local '("Verbatim" "lstlisting"))
(setq TeX-electric-sub-and-superscript t)
(LaTeX-add-environments '("IEEEeqnarray" LaTeX-env-label) '("IEEEeqnarray*" LaTeX-env-label) '("itemeqn" LaTeX-env-label))
(setq font-latex-math-environments '("IEEEeqnarray" "IEEEeqnarray*" "itemeqn"))
(setq texmathp-tex-commands '(("IEEEeqnarray" env-on) ("IEEEeqnarray*" env-on) ("itemeqn" env-on)))
(require 'reftex)
(add-to-list 'reftex-ref-style-default-list "Hyperref")
(setq reftex-label-alist '(AMSTeX ("IEEEeqnarray" ?e "eq:" "~\\eqref{%s}" t nil)))
(add-hook 'LaTeX-mode-hook (lambda ()
                             (turn-on-reftex)
                             (outline-minor-mode 1)
                             (setq outline-regexp "\\\\\\(sub\\)*section")
                             (outline-hide-body)))
(global-set-key (kbd "<insert>") 'outline-toggle-children)
(global-set-key (kbd "C-<insert>") 'outline-hide-body)

(setq lxkl/latex-label-stack '())
(defun lxkl/make-latex-label ()
  (interactive)
  (let* ((file-name (buffer-file-name))
         (file-name-nondirectory (file-name-nondirectory file-name))
         (file-name-sans-extension (file-name-sans-extension file-name-nondirectory))
         (directory-name (file-name-nondirectory (directory-file-name (file-name-directory file-name))))
         (label (concat directory-name "/" file-name-sans-extension)))
    (insert "\\label{" label ":}")
    (backward-char)))
(defun lxkl/copy-latex-label ()
  (interactive)
  (save-excursion
    (let ((line-start (line-beginning-position))
          (line-end (line-end-position)))
      (goto-char line-start)
      (if (cl-some (lambda (x) (re-search-forward (concat "\\\\" x "{\\([^}]+\\)}") line-end t))
                   '("label" "autoref" "autoref\\*" "ref" "eqref" "nameref"))
          (let ((label (match-string 1)))
            (push label lxkl/latex-label-stack)
            (message "Copied label: %s" label))
        (message "No label found on current line.")))))
(defun lxkl/ref-latex-label (cmd)
  (interactive)
  (save-excursion
    (insert (format "\\%s{%s}" cmd (pop lxkl/latex-label-stack)))))
(defmacro lxkl/make-ref-latex-label (cmd)
  `(lambda () (interactive) (lxkl/ref-latex-label ,cmd)))
(global-set-key (kbd "<f13> C-l C-m") 'lxkl/make-latex-label)
(global-set-key (kbd "<f13> C-l C-c") 'lxkl/copy-latex-label)
(global-set-key (kbd "<f13> C-l C-a") (lxkl/make-ref-latex-label "autoref"))
(global-set-key (kbd "<f13> C-l C-r") (lxkl/make-ref-latex-label "ref"))
(global-set-key (kbd "<f13> C-l C-e") (lxkl/make-ref-latex-label "eqref"))
(global-set-key (kbd "<f13> C-l C-l") (lxkl/make-ref-latex-label "label"))

(use-package magit
  :bind
  (("<f13> g s" . magit-status)
   ("<f13> g l" . magit-list-repositories))
  :config
  (add-hook 'magit-status-mode-hook (lambda () (olivetti-mode 1)))
  (setq magit-diff-refine-hunk 'all)
  (setq magit-repository-directories '(("~/repos/" . 1)))
  (add-to-list 'magit-repolist-columns '("Dirty" 5 magit-repolist-column-dirty ((:right-align t)))))

(use-package markup-faces
  :load-path "markup-faces"
  :defer t)

(use-package neotree
  :bind
  (("<f13> n t" . neotree-toggle)
   ("<f13> n n" . neotree-refresh))
  :config
  (setq neo-theme 'nerd)
  (setq neo-autorefresh nil))

(use-package notmuch
  :bind
  (("<f13> m" . notmuch))
  :config
  (setq mm-inlined-types (remove "image/.*" mm-inlined-types))
  (add-to-list 'mm-inhibit-file-name-handlers 'openwith-file-handler)
  (add-hook 'message-setup-hook 'mml-secure-message-sign-pgpmime)
  (setq mml-secure-smime-sign-with-sender t)
  (setq mml-secure-openpgp-sign-with-sender t)
  (setq mml-secure-openpgp-encrypt-to-self t)
  (setq notmuch-address-command "notmuch-addrlookup")
  (setq notmuch-crypto-process-mime t)
  (setq notmuch-draft-save-plaintext t)
  (setq notmuch-print-mechanism 'notmuch-print-muttprint)
  (add-hook 'notmuch-message-mode-hook (lambda ()
                                         (olivetti-mode)
                                         (auto-fill-mode 0)
                                         (visual-line-mode 1)))
  (setq notmuch-saved-searches
        '((:name "unread 200 days" :query "date:200d..today AND tag:unread" :sort-order newest-first)
          (:name    "all 200 days" :query "date:200d..today"                :sort-order newest-first)))
  (setq notmuch-show-mark-read-tags '())
  (setq notmuch-draft-tags '("+draft" "-inbox" "-unread"))
  (defmacro lxkl/notmuch-keys (map fn)
    `(progn (define-key ,map "d" (lambda () (interactive) (,fn (list "-unread"))))
            (define-key ,map "i" (lambda () (interactive) (,fn (list "-flagged"))))
            (define-key ,map "I" (lambda () (interactive) (,fn (list "+unread" "+flagged"))))
            (define-key ,map "U" (lambda () (interactive) (,fn (list "+unread"))))))
  (lxkl/notmuch-keys notmuch-search-mode-map notmuch-search-tag)
  (lxkl/notmuch-keys notmuch-show-mode-map notmuch-show-tag)
  (lxkl/notmuch-keys notmuch-tree-mode-map notmuch-tree-tag))

(use-package octave-mode
  :mode ("\\.m\\'"))

(use-package olivetti
  :demand
  :diminish
  :config
  (setq olivetti-minimum-body-width 85)
  :bind ("<f13> O" . olivetti-mode))

(use-package openwith
  :config
  (defun lxkl/openwith-list (l) (concat "\\.\\(?:" (mapconcat 'identity l "\\|") "\\)\\'"))
  (setq openwith-associations
        `((,(lxkl/openwith-list '("djvu" "pdf")) "zathura" (file))
          (,(lxkl/openwith-list '("doc" "docx" "ods" "xlsx")) "soffice" (file))
          (,(lxkl/openwith-list '("bmp" "jpe?g" "png" "webp")) "eog" (file))
          (,(lxkl/openwith-list '("flac" "mp3")) "celluloid" (file))
          (,(lxkl/openwith-list '("avi" "mkv" "mp4" "mpe?g" "mts" "m4v" "webm" "wmv")) "celluloid" (file))))
  (openwith-mode t))

(use-package org
  :bind
  (("<f13> o l" . org-store-link)
   ("<f13> o a" . org-agenda)
   ("<f13> o c" . org-capture)
   :map org-mode-map
   ("C-c <return>" . org-open-at-point)
   ("<f13> o f" . lxkl/org-make-file)
   ("C-ü" . imenu)
   ("<f13> o L" . lxkl/org-toggle-link-display)
   ("<f13> o s" . lxkl-org-share/share)
   ("<f13> o S" . lxkl-org-share/share-all))
  :config
  (add-to-list 'load-path "/opt/lxkl/org-share")
  (require 'lxkl-org-share)
  (custom-set-faces
   `(org-level-1 ((t (:inherit outline-1 :height ,(expt 1.1 2)))))
   `(org-level-2 ((t (:inherit outline-2 :height ,(expt 1.1 1)))))
   `(org-level-3 ((t (:inherit outline-3 :height ,(expt 1.1 0))))))
  (setq org-todo-keywords '((sequence "TODO" "WAITING" "DONE")))
  (require 'which-func)
  (add-to-list 'which-func-functions
               (lambda () (interactive)
                 (when (eq major-mode 'org-mode)
                   (if (null (org-current-level))
                       ""
                     (let ((x (org-get-outline-path t)))
                       (if (< 1 (length x))
                           (format "%s / %s" (nth 0 x) (nth 1 x))
                         (car x)))))))
  (add-to-list 'org-modules 'org-notmuch 'org-eww)
  (add-hook 'org-mode-hook (lambda ()
                             (imenu-add-to-menubar "Imenu")
                             (which-function-mode 1)
                             (olivetti-mode)
                             (auto-fill-mode 0)
                             (visual-line-mode 1)
                             (org-indent-mode 1)
                             (org-bullets-mode 1)))
  (add-hook 'org-cycle-hook
            (lambda (state)
              (when (eq state 'children)
                (setq org-cycle-subtree-status 'subtree))))
  (setq org-cycle-emulate-tab 'white)
  (setq org-adapt-indentation nil)
  (setq org-startup-folded t)
  (setq org-catch-invisible-edits 'show-and-error)
  (setq org-hide-emphasis-markers nil)
  (setq org-list-allow-alphabetical t)
  (setq org-use-speed-commands t)
  (defun lxkl/org-toggle-link-display ()
    (interactive)
    (if org-descriptive-links
        (progn (org-remove-from-invisibility-spec '(org-link))
               (org-restart-font-lock)
               (setq org-descriptive-links nil))
      (progn (add-to-invisibility-spec '(org-link))
             (org-restart-font-lock)
             (setq org-descriptive-links t))))
  (org-link-set-parameters
   "rtmp"
   :export (lambda (path desc x y) (format "<a href=\"rtmp:%s\">%s</a>" path (or desc (format "rtmp:%s" path)))))
  (delete '("\\.pdf\\'" . default) org-file-apps)
  (add-to-list 'org-file-apps '("\\.pdf\\'" . "zathura %s"))
  (setq lxkl/org-file-main (concat org-directory "/main.org"))
  (setq lxkl/org-file-references (concat org-directory "/references.org"))
  (setq org-agenda-files (list lxkl/org-file-main))
  (setq org-duration-format '(("h") (special . 2)))
  (setq org-clock-display-default-range 'untilnow)
  (setq org-capture-templates
        '(("a" "Agenda Item" entry
           (file+headline "~/org/main.org" "Appointments and reminders")
           "** %?\n%t\n%i" :prepend t)))
  (setq org-agenda-span 500
        org-agenda-start-on-weekday nil
        org-agenda-start-day "-100d")
  (defun lxkl/org-insert-subheading-after ()
    (interactive)
    (let ((current-prefix-arg '(4)))
      (call-interactively 'org-insert-subheading)))
  (define-key org-mode-map (kbd "S-<return>") 'lxkl/org-insert-subheading-after)
  (setq org-refile-targets (list (cons lxkl/org-file-main (cons :level 1))
                                 (cons lxkl/org-file-references (cons :level 1))))
  (setf (alist-get 'file org-link-frame-setup) 'find-file)
  (defun lxkl/org-find-main () (interactive) (find-file lxkl/org-file-main))
  (defun lxkl/org-find-references () (interactive) (find-file lxkl/org-file-references))
  (defun lxkl/org-make-file () (interactive)
         (let* ((link-text (read-string "Link text: "))
                (id (format "%s%04x%04x" (if (string= link-text "") "" (concat link-text " ")) (random (expt 16 4)) (random (expt 16 4))))
                (link-target (concat "file:file/" id ".org")))
           (insert (if (string= link-text "") (concat "[[" link-target "]]")
                     (concat "[[" link-target "][" link-text "]]")))))
  (org-babel-do-load-languages 'org-babel-load-languages '((R . t))))

(use-package paredit
  :config
  (add-hook 'emacs-lisp-mode-hook (lambda () (paredit-mode 1)))
  (add-hook 'scheme-mode-hook (lambda () (paredit-mode 1)))
  (add-hook 'lisp-mode-hook (lambda () (paredit-mode 1))))

(use-package projectile
  :bind-keymap
  (("<f13> p" . projectile-command-map))
  :bind
  (("C-S-<return>" . projectile-switch-to-buffer))
  (:map projectile-command-map
        ("P" . lxkl/projectile-switch-project-find-file))
  :config
  (projectile-mode 1)
  (defun lxkl/projectile-switch-project-find-file ()
    (interactive)
    (let ((projectile-switch-project-action 'projectile-find-file))
      (projectile-switch-project)))
  (setq projectile-switch-project-action 'projectile-switch-to-buffer)
  (setq projectile-completion-system 'ivy))

(use-package rainbow-delimiters
  :config
  (add-hook 'LaTeX-mode-hook (lambda () (rainbow-delimiters-mode 1)))
  (add-hook 'emacs-lisp-mode-hook (lambda () (rainbow-delimiters-mode 1)))
  (add-hook 'scheme-mode-hook (lambda () (rainbow-delimiters-mode 1)))
  (add-hook 'lisp-mode-hook (lambda () (rainbow-delimiters-mode 1))))

(use-package slime
  :config
  (setq inferior-lisp-program "sbcl"))

(use-package smart-mode-line
  :config
  (setq sml/no-confirm-load-theme t)
  (setq sml/theme 'respectful)
  (sml/setup))

(use-package so-long
  :config
  (global-so-long-mode 1))

(use-package swiper
  :bind
  (("C-s" . swiper)))

(use-package vterm
  :bind
  (("<f13> t" . vterm))
  :config
  (setq vterm-shell "/usr/bin/fish"))

(use-package web-mode
  :mode ("\\.erb\\'" "\\.html?\\'")
  :config
  (setq web-mode-engines-alist '(("erb" . "\\.erb\\'")))
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2))

(use-package modus-themes
  :load-path "/opt/lxkl/foreign/modus-themes"
  :demand
  :config
  (setq modus-themes-bold-constructs nil
        modus-themes-italic-constructs t)
  (defadvice load-theme (before theme-dont-propagate activate)
    (mapc #'disable-theme custom-enabled-themes))
  (load-theme 'modus-operandi t))

;;;
;;; Startup
;;;
